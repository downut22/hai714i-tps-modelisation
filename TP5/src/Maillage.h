#ifndef MAILLAGE_H
#define MAILLAGE_H

#include <cmath>
#include <iostream>
#include <cassert>
#include <cstdlib>

#include <map>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include "src/Vec3.h"

using namespace std;

struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres indices des sommets du triangle:
    unsigned int v[3];
};


struct BoundingBox
{
    Vec3 min;
    Vec3 max;

    Vec3 deltas(){
        return max-min;
    }

    void espilonize()
    {
        min -= Vec3(epsilon,epsilon,epsilon);
        max += Vec3(epsilon,epsilon,epsilon);
    }

    const float epsilon = 0.1;
};



struct Grid{

    Vec3* points; int count;

    vector<vector<Vec3>> vertices;
    vector<vector<Vec3>> normals;

    int width; int height; int length; float resolution;

    int idOf(Vec3 p)
    {
        return idOf((int)p[0],(int)p[1],(int)p[2]);
    }
    int idOf(int x, int y,int z)
    {
        return x + (y * width) + (z * width * height);
    }

    Vec3 get(int x, int y,int z)
    {
        return points[idOf(x,y,z)];
    }

    void set(int x, int y,int z, Vec3 v)
    {
        points[idOf(x,y,z)] = v;
    }

    Vec3 indexOf(Vec3 pos, BoundingBox box)
    {
        pos -= box.min;

        pos[0] = (int)round((pos[0] / box.deltas()[0]) * width);
        pos[1] = (int)round((pos[1] / box.deltas()[1]) * height);
        pos[2] = (int)round((pos[2] / box.deltas()[2]) * length);

        return pos;
    }


    void resize(int w,int h,int l)
    {
        width = w; height = h; length = l;
        points = new Vec3[w*h*l];
        vertices.resize(w*h*l);
        normals.resize(w*h*l);
        count = w*l*h;
    }
};

struct Mesh{
    std::vector< Vec3 > vertices; //array of mesh vertices positions
    std::vector< Vec3 > normals; //array of vertices normals useful for the display
    std::vector< Triangle > triangles; //array of mesh triangles
    std::vector< Vec3 > triangle_normals; //triangle normals to display face normals

    Vec3 get_normal(Vec3 x0, Vec3 x1, Vec3 x2)
    {
        Vec3 v0 = x0 - x2;
        Vec3 v1 = x1 - x2;
        Vec3 n = Vec3::cross(v0, v1);

        n.normalize();
        return n;
    }

    float get_area(Vec3 a, Vec3 b, Vec3 c)
    {
        float e1 = Vec3::cross(b-a,c-a).length() / 2.0; 
        return e1;
    }

    //Compute face normals for the display
    void computeTrianglesNormals(){

        triangle_normals.clear();
        triangle_normals.resize(triangles.size());

        for(int f = 0; f < triangles.size();f++)
        {
            Vec3 normal = get_normal(vertices[triangles[f][0]],vertices[triangles[f][1]],vertices[triangles[f][2]]);
            triangle_normals.push_back(normal);
        }
    }

    //Compute vertices normals as the average of its incident faces normals
    void computeVerticesNormals( int weight_type ){

        normals.clear();
        normals.resize(vertices.size());

        for(int v = 0; v < vertices.size();v++)
        {
            normals[v] = Vec3(0,0,0);
        }

        for(int f = 0; f < triangles.size();f++)
        {
            float weight;

            if(weight_type <= 1)
            {                    
               if(weight_type == 0)
                {
                    weight = 1;
                }
               else
               {
                    weight = get_area(vertices[triangles[f][0]],vertices[triangles[f][1]],vertices[triangles[f][2]]);
                }

                Vec3 norm = weight* triangle_normals[f] ;

                normals[triangles[f][0]] += norm;
                normals[triangles[f][1]] += norm;
                normals[triangles[f][2]] += norm;
            }
            else if(weight_type == 2)
            {
                for(int p = 0; p < 3; p++)
                {
                    int p2 = p+1;
                    if(p2 >=3){p2=0;}

                    int p3 = p2+1;
                    if(p3 >= 3){p3 = 0;}

                    Vec3 e1 = (vertices[triangles[f][p2]]-vertices[triangles[f][p]]); e1.normalize();   //Edge 1
                    Vec3 e2 = (vertices[triangles[f][p3]]-vertices[triangles[f][p]]); e2.normalize();  //Edge 2

                    weight = Vec3::dot(e1,e2); //Angle

                    Vec3 norm = weight* triangle_normals[f] ;
                    normals[triangles[f][p]] += norm;
                }
            }
        }

        for(int v = 0; v < normals.size();v++)
        {
            normals[v].normalize();
        }

    }

    void computeNormals(int weight_type)
    {
        computeTrianglesNormals();
        computeVerticesNormals(weight_type);
    }
};

class Maillage
{
public:
    Mesh mesh;
    BoundingBox box;
    Grid grid;

    void computeBounds()
    {
        box.min = Vec3(FLT_MAX,FLT_MAX,FLT_MAX);
        box.max = Vec3(FLT_MIN,FLT_MIN,FLT_MIN);

        for(int i = 0; i < mesh.vertices.size();i++)
        {
            box.min[0] = min(box.min[0],mesh.vertices[i][0]);
            box.min[1] = min(box.min[1],mesh.vertices[i][1]);
            box.min[2] = min(box.min[2],mesh.vertices[i][2]);

            box.max[0] = max(box.max[0],mesh.vertices[i][0]);
            box.max[1] = max(box.max[1],mesh.vertices[i][1]);
            box.max[2] = max(box.max[2],mesh.vertices[i][2]);
        }

        box.espilonize();
    }

    void computeGrid(float voxelSize)
    {
        grid.resolution = voxelSize;

        int xCount = box.deltas()[0] / voxelSize;
        int yCount = box.deltas()[1] / voxelSize;
        int zCount = box.deltas()[2] / voxelSize;

        grid.resize(xCount,yCount,zCount);

        for(int x = 0; x < xCount; x++)
        {
            for(int y = 0; y < yCount; y++)
            {
                for(int z = 0; z < zCount; z++)
                {
                    grid.set(x,y,z,Vec3(
                        (voxelSize/2.0) + box.min[0] +  x*voxelSize,
                        (voxelSize/2.0) +box.min[1] + y*voxelSize,
                        (voxelSize/2.0) +box.min[2] + z*voxelSize));
                }
            }
        }
    }

    void voxelize()
    {
        std::map<int,vector<Vec3>> pointMap;
        std::map<int,vector<Vec3>> normalMap;

        for(int i = 0; i < mesh.vertices.size();i++)
        {
            int id = grid.idOf(grid.indexOf(mesh.vertices[i],box));

            if(pointMap.count(id) < 1){
                pointMap[id] = vector<Vec3>();
            }
            pointMap[id].push_back(mesh.vertices[i]);

            if(!normalMap.count(id) < 1){
                normalMap[id] = vector<Vec3>();
            }
            normalMap[id].push_back(mesh.normals[i]);
        }

        vector<Triangle> tris;
        vector<Vec3> verts;
        vector<Vec3> norms;

        std::map<int,int> reduce;

        int counter = 0;
        for(std::map<int,vector<Vec3>>::iterator it = pointMap.begin(); it != pointMap.end();it++)
        {
            int id = it->first;

            Vec3 n = Vec3(0,0,0);
            for(int i = 0 ; i < normalMap[id].size();i++)
            {
                n += normalMap[id][i];
            }
            n.normalize();

            norms.push_back(n);

            Vec3 p = Vec3(0,0,0);
            for(int i = 0 ; i < pointMap[id].size();i++)
            {
                p += pointMap[id][i];
            }
            p /= pointMap[id].size();

            verts.push_back(p);

            reduce[id] = counter; counter++;
        }

        for(int i = 0; i < mesh.triangles.size();i++)
        {
            int id1 = grid.idOf(grid.indexOf(mesh.vertices[mesh.triangles[i][0]],box));
            int id2 = grid.idOf(grid.indexOf(mesh.vertices[mesh.triangles[i][1]],box));
            int id3 = grid.idOf(grid.indexOf(mesh.vertices[mesh.triangles[i][2]],box));

            id1 = reduce[id1]; id2 = reduce[id2]; id3 = reduce[id3];

            if(id1 != id2 && id2 != id3)
            {
                tris.push_back(Triangle(id1,id2,id3));
            }
        }

        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.normals = norms;
    }

    void simplify(float voxelSize)
    {
        computeBounds();

        computeGrid(voxelSize);

        voxelize();
    }
};

#endif
