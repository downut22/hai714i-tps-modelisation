#!/bin/sh
bindir=$(pwd)
cd /home/e20190014299/Bureau/Modelisation/hai714i-tps-modelisation/TP6/src/
export 

if test "x$1" = "x--debugger"; then
	shift
	if test "xYES" = "xYES"; then
		echo "r  " > $bindir/gdbscript
		echo "bt" >> $bindir/gdbscript
		/usr/bin/gdb -batch -command=$bindir/gdbscript --return-child-result /home/e20190014299/Bureau/Modelisation/hai714i-tps-modelisation/TP6/build/gmap 
	else
		"/home/e20190014299/Bureau/Modelisation/hai714i-tps-modelisation/TP6/build/gmap"  
	fi
else
	"/home/e20190014299/Bureau/Modelisation/hai714i-tps-modelisation/TP6/build/gmap"  
fi
