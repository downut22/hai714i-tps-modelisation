// #include "gmap.hpp"
#include <stdio.h>
#include <string.h>
#ifdef GMAP_CORE
/*------------------------------------------------------------------------*/

/* 
    Create a new dart and return its id. 
    Set its alpha_i to itself (fixed points) 
    Use maxid to determine the new id. Dont forget to increment it.
*/
GMap::id_t GMap::add_dart()
{
	id_t dart = maxid;
	alphas[dart] = alpha_container_t(dart,dart,dart);
	maxid++;
	return dart;
}

/*------------------------------------------------------------------------*/


// Return the application of the alpha_deg on dart
GMap::id_t GMap::alpha(degree_t degree, id_t dart) const
{
	assert(degree < 3);
	assert(dart < maxid);
	return alphas.at(dart).values[degree];
}

// Return the application of a composition of alphas on dart
GMap::id_t GMap::alpha(degreelist_t degrees, id_t dart) const
{
	for(int i = degrees.size() - 1 ; i >= 0;i--)
	{
		assert(degrees[i] < 3);
		assert(dart < maxid);
		dart = alphas.at(dart).values[degrees[i]];
	}
	return dart;
}


//  Test if dart is free for alpha_degree (if it is a fixed point) 
bool GMap::is_free(degree_t degree, id_t dart) const
{
	assert(degree < 3);
	assert(dart < maxid);
    return alphas.at(dart).values[degree] == dart;
}

/*------------------------------------------------------------------------*/


// Link the two darts with a relation alpha_degree if they are both free.
bool GMap::link_darts(degree_t degree, id_t dart1, id_t dart2)
{	
	assert(degree < 3);
	assert(dart1 < maxid);
	assert(dart2 < maxid);
	if(is_free(degree,dart1) || is_free(degree,dart2))
	{
		alphas.at(dart1).values[degree] = dart2;
		alphas.at(dart2).values[degree] = dart1;
		return true;
	}
    return false;
}


/*------------------------------------------------------------------------*/

/*
        Test the validity of the structure. 
        Check if alpha_0 and alpha_1 are involutions with no fixed points.
        Check if alpha_2 is an involution.
        Check if alpha_0 o alpha_2 is an involution
*/
bool GMap::is_valid() const
{
	for(id_t i = 0 ; i < maxid;i++)
	{
		if(is_free(0,i)) return false;
		if(is_free(1,i)) return false;

		if(alphas.at(alphas.at(i).values[0]).values[0] != i) return false;
		if(alphas.at(alphas.at(i).values[1]).values[1] != i) return false;
		if(alphas.at(alphas.at(i).values[2]).values[2] != i) return false;

		if(alphas.at(alphas.at(alphas.at(alphas.at(i).values[2]).values[0]).values[2]).values[0] != i) return false;
	}

    return true;
}

/*------------------------------------------------------------------------*/


/* 
    Return the orbit of dart using a list of alpha relation.
    Example of use : gmap.orbit([0,1],0).
*/
GMap::idlist_t GMap::orbit(const degreelist_t& a_list, id_t dart) const
{
	idlist_t result;
	idset_t marked;
	idlist_t toprocess = {dart};

	while(toprocess.size() > 0)
	{
		id_t b = toprocess.at(toprocess.size()-1); toprocess.pop_back();

		if(marked.count(b) <= 0)
		{
			result.push_back(b);
			marked.insert(b);

			for(id_t i = 0; i < a_list.size();i++)
			{
				toprocess.push_back(alpha(a_list.at(i),b));
			}	
		}
	}

	return result;
}

/*
    Return the ordered orbit of dart using a list of alpha relations by applying
    repeatingly the alpha relations of the list to dart.
    Example of use. gmap.orderedorbit([0,1],0).
    Warning: No fixed point for the given alpha should be contained.
*/
GMap::idlist_t GMap::orderedorbit(const degreelist_t& list_of_alpha_value, id_t dart) const
{
	idlist_t result;
	id_t a_index = 0;
	id_t b = dart;

	do
	{
		result.push_back(b);
		id_t a_suiv = list_of_alpha_value.at(a_index);
		a_index = (a_index+1) % list_of_alpha_value.size();
		b = alpha(a_suiv,b);

	}while(b != dart);

	return result;
}


/*
    Sew two elements of degree 'degree' that start at dart1 and dart2.
    Determine first the orbits of dart to sew and heck if they are compatible.
    Sew pairs of corresponding darts
    # and if they have different embedding  positions, merge them. 
*/
bool GMap::sew_dart(degree_t degree, id_t dart1, id_t dart2)
{
	if(degree == 1)
	{
		link_darts(1,dart1,dart2);
	}
	else{

		idlist_t orbit1,orbit2;

		if(degree == 0)
		{
			orbit1 = orderedorbit({2},dart1);
			orbit2 = orderedorbit({2},dart2);
		}
		else{
			orbit1 = orderedorbit({0},dart1);
			orbit2 = orderedorbit({0},dart2);
		}

		if(orbit1.size() != orbit2.size()){return false;}

		for(id_t i = 0; i < orbit1.size();i++)
		{
			link_darts(degree,orbit1.at(i),orbit2.at(i));
		}

	}    	

	return true;
}

// Compute the Euler-Poincare characteristic of the subdivision
int GMap::eulercharacteristic() const
{
    return elements(0).size() - elements(1).size() + elements(2).size();
}

#endif
/*------------------------------------------------------------------------*/


/*
    Check if a dart of the orbit representing the vertex has already been 
    associated with a value in propertydict. If yes, return this dart, else
    return the dart passed as argument.
*/



template<class T>
GMap::id_t EmbeddedGMap<T>::get_embedding_dart(id_t dart) const
{
   	idlist_t darts = orbit({1,2},dart);

   	for(int i = 0; i < darts.size();i++)
   	{
   		if(properties.count(darts[i]) > 0){return darts[i];}
   	}

    return dart;
}


/*------------------------------------------------------------------------*/

#ifdef GMAP_CORE

GMap3D GMap3D::dual()
{
}


/*------------------------------------------------------------------------*/

#endif