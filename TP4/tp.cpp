// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"

enum DisplayMode{ WIRE=0, SOLID=1, LIGHTED_WIRE=2, LIGHTED=3 };

struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres indices des sommets du triangle:
    unsigned int v[3];
};

//Basis ( origin, i, j ,k )
struct Basis {
    inline Basis ( Vec3 const & i_origin,  Vec3 const & i_i, Vec3 const & i_j, Vec3 const & i_k) {
        origin = i_origin; i = i_i ; j = i_j ; k = i_k;
    }

    inline Basis ( ) {
        origin = Vec3(0., 0., 0.);
        i = Vec3(1., 0., 0.) ; j = Vec3(0., 1., 0.) ; k = Vec3(0., 0., 1.);
    }
    Vec3 operator [] (unsigned int ib) {
        if(ib==0) return i;
        if(ib==1) return j;
        return k;}

    Vec3 origin;
    Vec3 i;
    Vec3 j;
    Vec3 k;
};
Basis basis;

bool display_normals;
bool display_smooth_normals;
bool display_mesh;
bool display_basis;
DisplayMode displayMode;
int weight_type;

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;

// ------------------------------------
// Application initialization
// ------------------------------------
void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glDisable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

    display_normals = false;
    display_mesh = true;
    display_smooth_normals = true;
    displayMode = LIGHTED;
    display_basis = false;
}

//-------------------------------------------------------------------------------------
//-------------------SURFACE--------------------------------
//-------------------------------------------------------------------------------------

struct Curve{
    Vec3* curve;
    int nbPoints;

    void resize(int count)
    {
    	curve = new Vec3[count]; nbPoints = count;
    }
};

struct Surface{
    Curve* curves;
    int nbCurves;

    void resize(int count){ curves = new Curve[count]; nbCurves = count;}
};

struct Grid{
	Vec3* points;
	int width; int height;

	Vec3 get(int x, int y)
	{
		return points[x + (y * width)];
	}

	void set(int x, int y, Vec3 v)
	{
		points[x + (y * width)] = v;
	}

	void resize(int w,int h)
	{
		width = w; height = h;
		points = new Vec3[w*h];
	}
};

Curve curve; Curve curve2;
Vec3* controlPoints; int nbControl;

Grid grid;
Surface surface;


int factorial(int x)
{
	if(x <= 1){return 1;}

	int res = x;
	x--;

	while(x > 0)
	{
		res *= x;
		x--;
	}

	return res;
}

float bernstein(float u, int i, int n)
{
	float res = 0;

	res += factorial(n) / (float)(factorial(i)*factorial(n-i));
	res *= pow(u,i);
	res *= pow(1.0-u,n-i);

	return res;
}

Vec3* bezierCurveByBernstein(Vec3* cPoints,int nbC,int nbU)
{
	Vec3* l = new Vec3[nbU];

	float unit = 1.0/(float)(nbU);

	for(int i = 0; i < nbU;i++)
	{
		float u = i * unit;

		Vec3 v = Vec3(0,0,0);
		for(int c = 0; c < nbC;c++)
		{
			v+= bernstein(u,c,nbC-1)*cPoints[c];
		}

		l[i] = v;
	}

	return l;
}

Vec3 casteljau(Vec3* cPoints, int c,float u,int k,int i)
{
	if(k==0){
		return cPoints[c];
	}

	return (	(1.0-u)*casteljau(cPoints,c,u,k-1,i) + 
				(u)*casteljau(cPoints,c+1,u,k-1,i));
}

Vec3* bezierCurveByCasteljau(Vec3* cPoints,int nbC,int nbU,int k)
{
	Vec3* l = new Vec3[nbU];

	float unit = 1.0/(float)(nbU);

	for(int i = 0; i < nbU;i++)
	{
		float u = i * unit;

		Vec3 v = bernstein(u,0,0)*casteljau(cPoints,0,u,k,i);
		
		l[i] = v;	
	}

	return l;
}

Vec3* hermiteCubicCurve(Vec3 p0, Vec3 p1, Vec3 v0, Vec3 v1, int nbU)
{
	Vec3* l = new Vec3[nbU];

	float unit = 1.0/(float)(nbU);

	Vec3 d = p0;
	Vec3 c = v0;
	Vec3 b = (-3 * p0) + (3 * p1) - (2 * v0) - (v1);
	Vec3 a = (2*p0) - (2*p1) + (v0) + (v1);

	for(int i = 0; i < nbU;i++)
	{
		float u = i * unit;

		l[i] = Vec3(
				(a[0] * pow(u,3)) + (b[0] * pow(u,2)) + (c[0] * u) + d[0],
				(a[1] * pow(u,3)) + (b[1] * pow(u,2)) + (c[1] * u) + d[1],
				(a[2] * pow(u,3)) + (b[2] * pow(u,2)) + (c[2] * u) + d[2]
			);
	}

	return l;
}

Surface cylindricSurface(Curve curve,Vec3 dir,int curveCount)
{
    Surface s; s.resize(curveCount);

    Vec3 dirUnit = dir / (float)curveCount;

    for(int i = 0; i < curveCount;i++)
    {
    	s.curves[i].resize(curve.nbPoints);

    	for(int v = 0; v < curve.nbPoints;v++)
    	{
    		s.curves[i].curve[v] = curve.curve[v] + (i * dirUnit);
    	}
    }

    return s;
}

Surface doubleSurface(Curve curve1, Curve curve2)
{
	Surface s; s.resize(curve2.nbPoints);

    for(int i = 0; i < curve2.nbPoints;i++)
    {
    	s.curves[i].resize(curve.nbPoints);

    	for(int v = 0; v < curve.nbPoints;v++)
    	{
    		s.curves[i].curve[v] = curve.curve[v] + curve2.curve[i];
    	}
    }

    return s;
}

Surface ruledSurface(Curve curve1, Curve curve2,int nbU)
{
	Surface s; s.resize(curve2.nbPoints);

    for(int i = 0; i < curve.nbPoints;i++)
    {
    	s.curves[i].resize(nbU);

    	for(int u = 0; u < nbU; u++)
    	{
    		float uu = u / (float)nbU;

    		s.curves[i].curve[u] = ((1-uu)*curve.curve[i]) + (uu*curve2.curve[i]);
    	}
    }

    return s;
}

Surface bezierSurface(Grid grid, int nbX, int nbY)
{
	Surface s; s.resize(nbX);
	for(int y = 0; y < nbX;y++){s.curves[y].resize(nbY);}

	for(int x = 0; x < nbX; x++)
	{
		for(int y = 0; y < nbY; y++)
		{
			float u = x / (float)(nbX-1);
			float v = y / (float)(nbY-1);

			Vec3 p = Vec3(0,0,0);

			for(int i = 0; i < grid.width;i++)
			{
				for(int j = 0; j < grid.height; j++)
				{
					p += bernstein(u,i,grid.width-1) * bernstein(v,j,grid.height-1) * grid.get(i,j);
				}
			}

			s.curves[x].curve[y] = p;
		}
	}

	return s;
}

void drawCurve(Curve curve, Vec3 color)
{
	glBegin(GL_LINE_STRIP);

	glColor3f(color[0],color[1],color[2]);


	for(int i = 0; i < curve.nbPoints;i++)
	{
 	 	glVertex3f(curve.curve[i][0],curve.curve[i][1],curve.curve[i][2]);
  	}

	glEnd();
}

void drawSurfaceLines(Surface surface,Vec3 color)
{
	glColor3f(color[0],color[1],color[2]);

	for(int i = 0 ; i < surface.curves[0].nbPoints;i++)
	{	
		glBegin(GL_LINE_STRIP);

		for(int v = 0 ; v < surface.nbCurves;v++)
		{
			glVertex3f(	surface.curves[v].curve[i][0],
						surface.curves[v].curve[i][1],
						surface.curves[v].curve[i][2]);
		}

		glEnd();
	}
}

void drawSurfaceCurves(Surface surface, Vec3 color)
{
	for(int i = 0; i < surface.nbCurves;i++)
	{
		drawCurve(surface.curves[i],color);
	}
}


void drawSurface(Surface surface, Vec3 color){

	drawSurfaceCurves(surface,color);

	drawSurfaceLines(surface,color);
}


void drawPoints(std::vector<Vec3> v, Vec3 color)
{
	glBegin(GL_POINTS);
	glColor3f(color[0],color[1],color[2]);
	glPointSize(100);
	for(int i = 0 ;i< v.size();i++)
	{
		glVertex3f(v[i][0],v[i][1],v[i][2]);
	}
	
	glEnd();
}

void drawGrid(Grid grid,Vec3 color)
{
	glColor3f(color[0],color[1],color[2]);

	for(int x = 0; x < grid.width;x++)
	{
		glBegin(GL_LINE_STRIP);
		for(int y = 0; y < grid.height; y++)
		{
			glVertex3f(grid.get(x,y)[0],grid.get(x,y)[1],grid.get(x,y)[2]);
		}
		glEnd();
	}

	for(int x = 0; x < grid.width;x++)
	{
		glBegin(GL_LINE_STRIP);
		for(int y = 0; y < grid.height; y++)
		{
			glVertex3f(grid.get(y,x)[0],grid.get(y,x)[1],grid.get(y,x)[2]);
		}
		glEnd();
	}
}

//Draw fonction
void draw () {

    
    if(displayMode == LIGHTED || displayMode == LIGHTED_WIRE){

        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_LIGHTING);
        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glDisable (GL_LIGHTING);

    }  else if(displayMode == SOLID ){
        glDisable (GL_LIGHTING);
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

    }

    drawCurve(curve,Vec3(1,1,1));
 	drawCurve(curve2,Vec3(1,1,1));

    //drawGrid(grid,Vec3(1,1,1));

   	drawSurface(surface,Vec3(0.3,0.3,1));

}

void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

//Mouse events
void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }

    idle ();
}

//Mouse motion, update camera
void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}

// ------------------------------------
// Start of graphical application
// ------------------------------------
int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP HAI702I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    //glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    //key ('?', 0, 0);

    displayMode = WIRE;

    //---------- Curve definition --------------
	controlPoints = new Vec3[4];
	controlPoints[0] = Vec3(0,0,0);
	controlPoints[1] = Vec3(0.2,0.5,0);
	controlPoints[2] = Vec3(0.5,-0.5,0);
	controlPoints[3] = Vec3(1,0,0);
	nbControl = 4;
    curve.nbPoints = 100;
	curve.curve = bezierCurveByCasteljau(controlPoints,nbControl,curve.nbPoints,3);
    //----------

    Vec3 dir = Vec3(0,0,1);
    surface = cylindricSurface(curve,dir,20);

    //-----------
	controlPoints = new Vec3[4];
	controlPoints[0] = Vec3(0,0,0.5);
	controlPoints[1] = Vec3(0.5,0.2,0.5);
	controlPoints[2] = Vec3(0.6,0.5,0.5);
	controlPoints[3] = Vec3(1,0,0.5);
	nbControl = 4;

	curve2.nbPoints = 100;
	curve2.curve = bezierCurveByCasteljau(controlPoints,nbControl,curve2.nbPoints,3);

	//----------

	surface = ruledSurface(curve,curve2,30);

	//----------------

	grid.resize(4,4);
	grid.set(0,0,Vec3(0,0,0));	grid.set(0,1,Vec3(0.3,0.2,0));	grid.set(0,2,Vec3(0.6,0.2,0));	grid.set(0,3,Vec3(1,0,0));
	grid.set(1,0,Vec3(0,0.2,0.3));	grid.set(1,1,Vec3(0.3,0.4,0.3));	grid.set(1,2,Vec3(0.6,0.4,0.3));	grid.set(1,3,Vec3(1,0.2,0.3));
	grid.set(2,0,Vec3(0,0.2,0.6));	grid.set(2,1,Vec3(0.3,0.4,0.6));	grid.set(2,2,Vec3(0.6,0.4,0.6));	grid.set(2,3,Vec3(1,0.2,0.6));
	grid.set(3,0,Vec3(0,0,1));	grid.set(3,1,Vec3(0.3,0.2,1));	grid.set(3,2,Vec3(0.6,0.2,1));	grid.set(3,3,Vec3(1,0,1));

	//surface = bezierSurface(grid,20,20);

    basis = Basis();

    glutMainLoop ();
    return EXIT_SUCCESS;
}

