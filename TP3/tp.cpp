// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"

enum DisplayMode{ WIRE=0, SOLID=1, LIGHTED_WIRE=2, LIGHTED=3 };

struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres indices des sommets du triangle:
    unsigned int v[3];
};


struct Mesh {
    std::vector< Vec3 > vertices; //array of mesh vertices positions
    std::vector< Vec3 > normals; //array of vertices normals useful for the display
    std::vector< Triangle > triangles; //array of mesh triangles
    std::vector< Vec3 > triangle_normals; //triangle normals to display face normals

    Vec3 get_normal(Vec3 x0, Vec3 x1, Vec3 x2)
    {
        Vec3 v0 = x0 - x2;
        Vec3 v1 = x1 - x2;
        Vec3 n = Vec3::cross(v0, v1);

        n.normalize();
        return n;
    }

    float get_area(Vec3 a, Vec3 b, Vec3 c)
    {
        float e1 = Vec3::cross(b-a,c-a).length() / 2.0; 
        return e1;
    }

    //Compute face normals for the display
    void computeTrianglesNormals(){

        triangle_normals.clear();
        triangle_normals.resize(triangles.size());

        for(int f = 0; f < triangles.size();f++)
        {
            Vec3 normal = get_normal(vertices[triangles[f][0]],vertices[triangles[f][1]],vertices[triangles[f][2]]);
            triangle_normals.push_back(normal);
        }
        //A faire : implémenter le calcul des normales par face
        //Attention commencer la fonction par triangle_normals.clear();
        //Iterer sur les triangles

        //La normal du triangle i est le resultat du produit vectoriel de deux ses arêtes e_10 et e_20 normalisé (e_10^e_20)
        //L'arete e_10 est représentée par le vecteur partant du sommet 0 (triangles[i][0]) au sommet 1 (triangles[i][1])
        //L'arete e_20 est représentée par le vecteur partant du sommet 0 (triangles[i][0]) au sommet 2 (triangles[i][2])

        //Normaliser et ajouter dans triangle_normales
    }

    //Compute vertices normals as the average of its incident faces normals
    void computeVerticesNormals( int weight_type ){

        normals.clear();
        normals.resize(vertices.size());

        for(int v = 0; v < vertices.size();v++)
        {
            normals[v] = Vec3(0,0,0);
        }

        for(int f = 0; f < triangles.size();f++)
        {
            float weight;

            if(weight_type <= 1)
            {                    
               if(weight_type == 0)
                {
                    weight = 1;
                }
               else
               {
                    weight = get_area(vertices[triangles[f][0]],vertices[triangles[f][1]],vertices[triangles[f][2]]);
                }

                Vec3 norm = weight* triangle_normals[f] ;

                normals[triangles[f][0]] += norm;
                normals[triangles[f][1]] += norm;
                normals[triangles[f][2]] += norm;
            }
            else if(weight_type == 2)
            {
                for(int p = 0; p < 3; p++)
                {
                    int p2 = p+1;
                    if(p2 >=3){p2=0;}

                    int p3 = p2+1;
                    if(p3 >= 3){p3 = 0;}

                    Vec3 e1 = (vertices[triangles[f][p2]]-vertices[triangles[f][p]]); e1.normalize();   //Edge 1
                    Vec3 e2 = (vertices[triangles[f][p3]]-vertices[triangles[f][p]]); e2.normalize();  //Edge 2

                    weight = Vec3::dot(e1,e2); //Angle

                    Vec3 norm = weight* triangle_normals[f] ;
                    normals[triangles[f][p]] += norm;
                }
            }
        }

        for(int v = 0; v < normals.size();v++)
        {
            normals[v].normalize();
        }

        //Utiliser weight_type : 0 uniforme, 1 aire des triangles, 2 angle du triangle

        //A faire : implémenter le calcul des normales par sommet comme la moyenne des normales des triangles incidents
        //Attention commencer la fonction par normals.clear();
        //Initializer le vecteur normals taille vertices.size() avec Vec3(0., 0., 0.)
        //Iterer sur les triangles

        //Pour chaque triangle i
        //Ajouter la normal au triangle à celle de chacun des sommets en utilisant des poids
        //0 uniforme, 1 aire du triangle, 2 angle du triangle

        //Iterer sur les normales et les normaliser


    }

    void computeNormals(int weight_type){
        computeTrianglesNormals();
        computeVerticesNormals(weight_type);
    }
};

//Transformation made of a rotation and translation
struct Transformation {
    Mat3 rotation;
    Vec3 translation;
};

//Basis ( origin, i, j ,k )
struct Basis {
    inline Basis ( Vec3 const & i_origin,  Vec3 const & i_i, Vec3 const & i_j, Vec3 const & i_k) {
        origin = i_origin; i = i_i ; j = i_j ; k = i_k;
    }

    inline Basis ( ) {
        origin = Vec3(0., 0., 0.);
        i = Vec3(1., 0., 0.) ; j = Vec3(0., 1., 0.) ; k = Vec3(0., 0., 1.);
    }
    Vec3 operator [] (unsigned int ib) {
        if(ib==0) return i;
        if(ib==1) return j;
        return k;}

    Vec3 origin;
    Vec3 i;
    Vec3 j;
    Vec3 k;
};

//Fonction à completer
void collect_one_ring (std::vector<Vec3> const & i_vertices,
                       std::vector< Triangle > const & i_triangles,
                       std::vector<std::vector<unsigned int> > & o_one_ring) 
{
    o_one_ring.clear();
    o_one_ring.resize(i_vertices.size());

    //Foreach triangle >
    for(int f = 0; f < i_triangles.size();f++)
    {
        //FOReach point of the triangle
        for(int p = 0; p < 3; p++)
        {
            int p2 = p+1;
            if(p2 >=3){p2=0;}

            int p3 = p2+1;
            if(p3 >= 3){p3 = 0;}

            //Check if this point isn't registered in the 'v' ring vector
            if(std::count(o_one_ring[i_triangles[f][p]].begin(),o_one_ring[i_triangles[f][p]].end(),i_triangles[f][p2]) <= 0)
            {
                o_one_ring[i_triangles[f][p]].push_back(i_triangles[f][p2]);
            }
            if(std::count(o_one_ring[i_triangles[f][p]].begin(),o_one_ring[i_triangles[f][p]].end(),i_triangles[f][p3]) <= 0)
            {
                o_one_ring[i_triangles[f][p]].push_back(i_triangles[f][p3]);
            }
        }
    }
}

//Fonction à completer
void compute_vertex_valences (const std::vector<Vec3> & i_vertices,
                              const std::vector< Triangle > & i_triangles,
                              std::vector<std::vector<unsigned int> > & o_one_ring,
                              std::vector<unsigned int> & o_valences ) 
{
    o_valences.clear();

    for(int v = 0; v < o_one_ring.size();v++)
    {        
        std::cout<<o_one_ring[v].size()<<std::endl;

        o_valences.push_back(o_one_ring[v].size());
    }
}



//Input mesh loaded at the launch of the application
Mesh mesh;
std::vector< float > mesh_valence_field; //normalized valence of each vertex

void compute_valence_field(std::vector<unsigned int> & o_valences)
 {
     mesh_valence_field.clear();

     int max = 0;
     for(int v = 0 ;v < o_valences.size();v++)
     {
        if(o_valences[v] > max){max = o_valences[v];}
     }

     for(int v = 0; v < o_valences.size();v++)
     {
        mesh_valence_field.push_back(o_valences[v] / (float)max);
     }
 }

Basis basis;

bool display_normals;
bool display_smooth_normals;
bool display_mesh;
bool display_basis;
DisplayMode displayMode;
int weight_type;

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;

// ------------------------------------
// File I/O
// ------------------------------------
bool saveOFF( const std::string & filename ,
              std::vector< Vec3 > const & i_vertices ,
              std::vector< Vec3 > const & i_normals ,
              std::vector< Triangle > const & i_triangles,
              std::vector< Vec3 > const & i_triangle_normals ,
              bool save_normals = true ) 
{
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl ;

    unsigned int n_vertices = i_vertices.size() , n_triangles = i_triangles.size();
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << i_vertices[v][0] << " " << i_vertices[v][1] << " " << i_vertices[v][2] << " ";
        if (save_normals) myfile << i_normals[v][0] << " " << i_normals[v][1] << " " << i_normals[v][2] << std::endl;
        else myfile << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << i_triangles[f][0] << " " << i_triangles[f][1] << " " << i_triangles[f][2]<< " ";
        if (save_normals) myfile << i_triangle_normals[f][0] << " " << i_triangle_normals[f][1] << " " << i_triangle_normals[f][2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}

void openOFF( std::string const & filename,
              std::vector<Vec3> & o_vertices,
              std::vector<Vec3> & o_normals,
              std::vector< Triangle > & o_triangles,
              std::vector< Vec3 > & o_triangle_normals,
              bool load_normals = true )
{
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open())
    {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if( magic_s != "OFF" )
    {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices , n_faces , dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    o_vertices.clear();
    o_normals.clear();

    for( int v = 0 ; v < n_vertices ; ++v )
    {
        float x , y , z ;

        myfile >> x >> y >> z ;
        o_vertices.push_back( Vec3( x , y , z ) );

        if( load_normals ) {
            myfile >> x >> y >> z;
            o_normals.push_back( Vec3( x , y , z ) );
        }
    }

    o_triangles.clear();
    o_triangle_normals.clear();
    for( int f = 0 ; f < n_faces ; ++f )
    {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if( n_vertices_on_face == 3 )
        {
            unsigned int _v1 , _v2 , _v3;
            myfile >> _v1 >> _v2 >> _v3;

            o_triangles.push_back(Triangle( _v1, _v2, _v3 ));

            if( load_normals ) {
                float x , y , z ;
                myfile >> x >> y >> z;
                o_triangle_normals.push_back( Vec3( x , y , z ) );
            }
        }
        else if( n_vertices_on_face == 4 )
        {
            unsigned int _v1 , _v2 , _v3 , _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            o_triangles.push_back(Triangle(_v1, _v2, _v3 ));
            o_triangles.push_back(Triangle(_v1, _v3, _v4));
            if( load_normals ) {
                float x , y , z ;
                myfile >> x >> y >> z;
                o_triangle_normals.push_back( Vec3( x , y , z ) );
            }

        }
        else
        {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
            myfile.close();
            exit(1);
        }
    }

}

// ------------------------------------
// Application initialization
// ------------------------------------
void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glDisable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

    display_normals = false;
    display_mesh = true;
    display_smooth_normals = true;
    displayMode = LIGHTED;
    display_basis = false;
}


// ------------------------------------
// Rendering.
// ------------------------------------

void drawVector( Vec3 const & i_from, Vec3 const & i_to ) {

    glBegin(GL_LINES);
    glVertex3f( i_from[0] , i_from[1] , i_from[2] );
    glVertex3f( i_to[0] , i_to[1] , i_to[2] );
    glEnd();
}

void drawAxis( Vec3 const & i_origin, Vec3 const & i_direction ) {

    glLineWidth(4); // for example...
    drawVector(i_origin, i_origin + i_direction);
}

void drawReferenceFrame( Vec3 const & origin, Vec3 const & i, Vec3 const & j, Vec3 const & k ) {

    glDisable(GL_LIGHTING);
    glColor3f( 0.8, 0.2, 0.2 );
    drawAxis( origin, i );
    glColor3f( 0.2, 0.8, 0.2 );
    drawAxis( origin, j );
    glColor3f( 0.2, 0.2, 0.8 );
    drawAxis( origin, k );
    glEnable(GL_LIGHTING);

}

void drawReferenceFrame( Basis & i_basis ) {
    drawReferenceFrame( i_basis.origin, i_basis.i, i_basis.j, i_basis.k );
}

typedef struct {
    float r;       // ∈ [0, 1]
    float g;       // ∈ [0, 1]
    float b;       // ∈ [0, 1]
} RGB;



RGB scalarToRGB( float scalar_value ) //Scalar_value ∈ [0, 1]
{
    RGB rgb;
    float H = scalar_value*360., S = 1., V = 0.85,
            P, Q, T,
            fract;

    (H == 360.)?(H = 0.):(H /= 60.);
    fract = H - floor(H);

    P = V*(1. - S);
    Q = V*(1. - S*fract);
    T = V*(1. - S*(1. - fract));

    if      (0. <= H && H < 1.)
        rgb = (RGB){.r = V, .g = T, .b = P};
    else if (1. <= H && H < 2.)
        rgb = (RGB){.r = Q, .g = V, .b = P};
    else if (2. <= H && H < 3.)
        rgb = (RGB){.r = P, .g = V, .b = T};
    else if (3. <= H && H < 4.)
        rgb = (RGB){.r = P, .g = Q, .b = V};
    else if (4. <= H && H < 5.)
        rgb = (RGB){.r = T, .g = P, .b = V};
    else if (5. <= H && H < 6.)
        rgb = (RGB){.r = V, .g = P, .b = Q};
    else
        rgb = (RGB){.r = 0., .g = 0., .b = 0.};

    return rgb;
}

void drawSmoothTriangleMesh( Mesh const & i_mesh , bool draw_field = false ) {
    glBegin(GL_TRIANGLES);
    for(unsigned int tIt = 0 ; tIt < i_mesh.triangles.size(); ++tIt) {

        for(unsigned int i = 0 ; i < 3 ; i++) {
            const Vec3 & p = i_mesh.vertices[i_mesh.triangles[tIt][i]]; //Vertex position
            const Vec3 & n = i_mesh.normals[i_mesh.triangles[tIt][i]]; //Vertex normal

            if( draw_field && mesh_valence_field.size() > 0 ){
                RGB color = scalarToRGB( mesh_valence_field[i_mesh.triangles[tIt][i]] );
                glColor3f( color.r, color.g, color.b );
            }
            glNormal3f( n[0] , n[1] , n[2] );
            glVertex3f( p[0] , p[1] , p[2] );
        }
    }
    glEnd();

}

void drawTriangleMesh( Mesh const & i_mesh , bool draw_field = false  ) {
    glBegin(GL_TRIANGLES);
    for(unsigned int tIt = 0 ; tIt < i_mesh.triangles.size(); ++tIt) {
        const Vec3 & n = i_mesh.triangle_normals[ tIt ]; //Triangle normal
        for(unsigned int i = 0 ; i < 3 ; i++) {
            const Vec3 & p = i_mesh.vertices[i_mesh.triangles[tIt][i]]; //Vertex position

            if( draw_field ){
                RGB color = scalarToRGB( mesh_valence_field[i_mesh.triangles[tIt][i]] );
                glColor3f( color.r, color.g, color.b );
            }
            glNormal3f( n[0] , n[1] , n[2] );
            glVertex3f( p[0] , p[1] , p[2] );
        }
    }
    glEnd();

}

void drawMesh( Mesh const & i_mesh , bool draw_field = false ){
    if(display_smooth_normals)
        drawSmoothTriangleMesh(i_mesh, draw_field) ; //Smooth display with vertices normals
    else
        drawTriangleMesh(i_mesh, draw_field) ; //Display with face normals
}

void drawVectorField( std::vector<Vec3> const & i_positions, std::vector<Vec3> const & i_directions ) {
    glLineWidth(1.);
    for(unsigned int pIt = 0 ; pIt < i_directions.size() ; ++pIt) {
        Vec3 to = i_positions[pIt] + 0.02*i_directions[pIt];
        drawVector(i_positions[pIt], to);
    }
}

void drawNormals(Mesh const& i_mesh){

    if(display_smooth_normals){
        drawVectorField( i_mesh.vertices, i_mesh.normals );
    } else {
        std::vector<Vec3> triangle_baricenters;
        for ( const Triangle& triangle : i_mesh.triangles ){
            Vec3 triangle_baricenter (0.,0.,0.);
            for( unsigned int i = 0 ; i < 3 ; i++ )
                triangle_baricenter += i_mesh.vertices[triangle[i]];
            triangle_baricenter /= 3.;
            triangle_baricenters.push_back(triangle_baricenter);
        }

        drawVectorField( triangle_baricenters, i_mesh.triangle_normals );
    }
}

//-------------------------------------------------------------------------------------
//-------------------CURVES--------------------------------
//-------------------------------------------------------------------------------------

Vec3* curve; int nbPoints;
Vec3* controlPoints; int nbControl;
Vec3* casteljauCurve;
std::vector<std::vector<Vec3>> casteljauDebug;

int factorial(int x)
{
	if(x <= 1){return 1;}

	int res = x;
	x--;

	while(x > 0)
	{
		res *= x;
		x--;
	}

	return res;
}

float bernstein(float u, int i, int n)
{
	float res = 0;

	res += factorial(n) / (float)(factorial(i)*factorial(n-i));
	res *= pow(u,i);
	res *= pow(1.0-u,n-i);

	return res;
}

Vec3* bezierCurveByBernstein(Vec3* cPoints,int nbC,int nbU)
{
	Vec3* l = new Vec3[nbU];

	float unit = 1.0/(float)(nbU);

	for(int i = 0; i < nbU;i++)
	{
		float u = i * unit;

		Vec3 v = Vec3(0,0,0);
		for(int c = 0; c < nbC;c++)
		{
			v+= bernstein(u,c,nbC-1)*cPoints[c];
		}

		l[i] = v;
	}

	return l;
}

Vec3 casteljau(Vec3* cPoints, int c,float u,int k,int i)
{
	if(k==0){
		return cPoints[c];
	}

	if(i==30)
	{
		for(int a = 0; a < casteljauDebug[k+1].size()-1;a++)
		{
			casteljauDebug[k].push_back(
				((1.0-u)*casteljauDebug[k+1][a]) + 
				(u*casteljauDebug[k+1][a+1]));
		}
	}

	return (	(1.0-u)*casteljau(cPoints,c,u,k-1,i) + 
				(u)*casteljau(cPoints,c+1,u,k-1,i));
}

Vec3* bezierCurveByCasteljau(Vec3* cPoints,int nbC,int nbU,int k)
{
	Vec3* l = new Vec3[nbU];

	float unit = 1.0/(float)(nbU);

	for(int i = 0; i < nbU;i++)
	{
		float u = i * unit;

		Vec3 v = bernstein(u,0,0)*casteljau(cPoints,0,u,k,i);
		
		l[i] = v;	
	}

	return l;
}

Vec3* hermiteCubicCurve(Vec3 p0, Vec3 p1, Vec3 v0, Vec3 v1, int nbU)
{
	Vec3* l = new Vec3[nbU];

	float unit = 1.0/(float)(nbU);

	Vec3 d = p0;
	Vec3 c = v0;
	Vec3 b = (-3 * p0) + (3 * p1) - (2 * v0) - (v1);
	Vec3 a = (2*p0) - (2*p1) + (v0) + (v1);

	for(int i = 0; i < nbU;i++)
	{
		float u = i * unit;

		l[i] = Vec3(
				(a[0] * pow(u,3)) + (b[0] * pow(u,2)) + (c[0] * u) + d[0],
				(a[1] * pow(u,3)) + (b[1] * pow(u,2)) + (c[1] * u) + d[1],
				(a[2] * pow(u,3)) + (b[2] * pow(u,2)) + (c[2] * u) + d[2]
			);
	}

	return l;
}

void drawCurve(Vec3* pointList,int pointCount, Vec3 color)
{
	glBegin(GL_LINE_STRIP);

	glColor3f(color[0],color[1],color[2]);


	for(int i = 0; i < pointCount;i++)
	{
 	 	glVertex3f(pointList[i][0],pointList[i][1],pointList[i][2]);
  	}

	glEnd();
}

void drawPoints(std::vector<Vec3> v, Vec3 color)
{
	glBegin(GL_POINTS);
	glColor3f(color[0],color[1],color[2]);
	glPointSize(100);
	for(int i = 0 ;i< v.size();i++)
	{
		glVertex3f(v[i][0],v[i][1],v[i][2]);
	}
	
	glEnd();
}

//Draw fonction
void draw () {


    if(displayMode == LIGHTED || displayMode == LIGHTED_WIRE){

        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_LIGHTING);

    }  else if(displayMode == WIRE){

        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glDisable (GL_LIGHTING);

    }  else if(displayMode == SOLID ){
        glDisable (GL_LIGHTING);
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

    }

    glColor3f(0.8,1,0.8);
    //drawMesh(mesh, true);

	//drawCurve(curve,nbPoints,Vec3(1.0,0.0,0.0));
	drawCurve(controlPoints,nbControl,Vec3(0.0,0.0,1.0));

    drawCurve(casteljauCurve,nbPoints,Vec3(1.0,0.0,0.0));

    drawCurve(&casteljauDebug[0][0],casteljauDebug[0].size(),Vec3(0.0,1.0,0.0));
    drawCurve(&casteljauDebug[1][0],casteljauDebug[1].size(),Vec3(0.0,1.0,1.0));
    drawCurve(&casteljauDebug[2][0],casteljauDebug[2].size(),Vec3(0.0,1.0,0.0));        
    drawCurve(&casteljauDebug[3][0],casteljauDebug[3].size(),Vec3(0.0,1.0,1.0));   

    if(displayMode == SOLID || displayMode == LIGHTED_WIRE){
        glEnable (GL_POLYGON_OFFSET_LINE);
        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth (1.0f);
        glPolygonOffset (-2.0, 1.0);

        glColor3f(0.,0.,0.);
        //drawMesh(mesh, false);

        glDisable (GL_POLYGON_OFFSET_LINE);
        glEnable (GL_LIGHTING);
    }



    glDisable(GL_LIGHTING);
    if(display_normals){
        glColor3f(1.,0.,0.);
        //drawNormals(mesh);
    }

    if( display_basis ){
        drawReferenceFrame(basis);
    }
    glEnable(GL_LIGHTING);


}

void changeDisplayMode(){
    if(displayMode == LIGHTED)
        displayMode = LIGHTED_WIRE;
    else if(displayMode == LIGHTED_WIRE)
        displayMode = SOLID;
    else if(displayMode == SOLID)
        displayMode = WIRE;
    else
        displayMode = LIGHTED;
}

void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

// ------------------------------------
// User inputs
// ------------------------------------
//Keyboard event
void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;


    case 'w': //Change le mode d'affichage
        changeDisplayMode();
        break;


    case 'b': //Toggle basis display
        display_basis = !display_basis;
        break;

    case 'n': //Press n key to display normals
        display_normals = !display_normals;
        break;

    case '1': //Toggle loaded mesh display
        display_mesh = !display_mesh;
        break;

    case 's': //Switches between face normals and vertices normals
        display_smooth_normals = !display_smooth_normals;
        break;

    case '+': //Changes weight type: 0 uniforme, 1 aire des triangles, 2 angle du triangle
        weight_type ++;
        if(weight_type == 3) weight_type = 0;
        mesh.computeVerticesNormals(weight_type); //recalcul des normales avec le type de poids choisi
        std::cout<<"weight_type : " << weight_type << std::endl;
        break;

    default:
        break;
    }
    idle ();
}

//Mouse events
void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }

    idle ();
}

//Mouse motion, update camera
void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}

// ------------------------------------
// Start of graphical application
// ------------------------------------
int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP HAI702I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    displayMode = WIRE;

    //-----------
    curve = new Vec3[4];
    curve[0] = Vec3(0,0,0);
    curve[1] = Vec3(0.5,0,0);
	curve[2] = Vec3(0.7,0.5,0);
	curve[3] = Vec3(0.4,0.8,0);
	nbPoints = 4;
    //----------

    //----------
    Vec3 p0 = Vec3(0,0,0);
    Vec3 p1 = Vec3(2,0,0);
    Vec3 v0 = Vec3(1,1,0);
    Vec3 v1 = Vec3(1,-1,0);
    nbPoints = 100;
	curve = hermiteCubicCurve(p0,p1,v0,v1,nbPoints);
    //----------

    //----------
	controlPoints = new Vec3[4];
	controlPoints[0] = Vec3(0,0,0);
	controlPoints[1] = Vec3(0.2,0.5,0);
	controlPoints[2] = Vec3(0.5,-0.5,0);
	controlPoints[3] = Vec3(1,0,0);
	nbControl = 4;

	nbPoints = 100;
   	curve = bezierCurveByBernstein(controlPoints,nbControl,nbPoints);

    //----------

    //----------
    casteljauDebug.resize(5);
    for(int i = 0; i < 4;i++)
    {
    	casteljauDebug[4].push_back(controlPoints[i]);
    }
	casteljauCurve = bezierCurveByCasteljau(controlPoints,nbControl,nbPoints,3);

    basis = Basis();

    glutMainLoop ();
    return EXIT_SUCCESS;
}

