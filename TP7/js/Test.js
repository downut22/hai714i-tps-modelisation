class Test {
    static defaultAction = "draw";
    cp; // control panel
    a;  // action
    d;  // displayer
    centre = new Coord2D(0, 0);
    referencePoint;
    states = { center: 2, comp: 3, smaller: -1, bigger: 1, equal: 0 };


    constructor(_action) {
        this.a = _action;
    }

    //priori simplicité (version draft: les clés des options ui sont utilisées telles quelles dans un if else infernal, beurk mais rapide, à revoir ?)
    //ui keys = tour, comp, tri, algo_dp, algo_jarvis, algo_graham
    run() {
        this.d.init();
        this.d.setModelSpace(...this.getModelSpace());
        this.d.drawPoints(this.getPoints(), false);

		var p1 = this.getPoints()[0];
        var p2 = this.getPoints()[1];
        var p3 = this.getPoints()[2];



       	try{
        	console.log(this.a);

        	if(this.a == "tour")
        	{
        			p1 = this.getPoints()[0];
        			p2 = this.getPoints()[1];
        			p3 = this.getPoints()[2];
        			var t = Coord2D.tour(p1,p2,p3);
       				this.d.drawTour(t,[p1,p2,p3],false);
        			console.log(">> Tour : " + t);
        			console.log(Coord2D.tour(p1,p2,p3));

        	}
        	else if(this.a == "comp")
        	{
        		var p = Coord2D.findMinIdx(this.getPoints());
        		console.log(p);
        		this.d.colors.save();
        		this.d.colors.fg = this.d.colors.left;
        		this.d.mDrawPoint(this.getPoints()[p]);
        		this.d.colors.restore();
        	}
        	else if(this.a == "algo_jarvis")
        	{
        		var env = new EnveloppeConvexe(this.getPoints(),1);
        		env.runAlgo();
        		var conv = env.getEnvConv();

        		console.log(conv);

        		this.d.drawEnveloppe(this.getPoints(),conv,false);
        	}
       	}catch(e)
       	{
       		console.log(e);
       	}
    }

    setUI(_controlPanel, _displayer) {
        this.cp = _controlPanel;
        this.d = _displayer;
    }

    changeAction(_newA) {
        this.a = _newA;
        this.run();
    }
    
    refresh() {
        this.run();
    }

    // made to fit the signature of setModelSpace@Displayer(minX, minY, maxX, maxY)
    getModelSpace() {
        return [
            this.cp.getDataSet().minX,
            this.cp.getDataSet().minY,
            this.cp.getDataSet().maxX,
            this.cp.getDataSet().maxY
        ]
    }

    getPoints() {
        return this.cp.getDataSet().getPoints();
    }

    renewData() {
        this.cp.renewData();
        this.refresh();
    }
}