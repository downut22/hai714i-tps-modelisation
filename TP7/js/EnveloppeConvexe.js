
class EnveloppeConvexe {
    static algo = { demiPlan: 0, jarvis: 1, graham: 2 };
    constructor(lesPoints, a) {
        this.algo = a ?? EnveloppeConvexe.algo.jarvis;
        this.points = lesPoints;
        this.envConv = new Array();
        this.runAlgo(this.algo);
    }

    getEnvConv() {
         return this.envConv;
    }

    updateEnvConvFromIndices(indices) {
       this.envConv = new Array();
       for (let i = 0; i < indices.length; i++) {
           this.envConv.push(this.points[indices[i]]);
        }      
    }

    runAlgo(idAlgo) {
        this.algo = idAlgo;
        switch (this.algo) {
            case EnveloppeConvexe.algo.demiPlan:
                this.envconv = this.algoDemiPlan(this.points);
                break;
            case EnveloppeConvexe.algo.jarvis:
                this.envconv = this.algoJarvis(this.points);
                break;
            case EnveloppeConvexe.algo.graham:
                this.envconv = this.algoGraham(this.points);
                break;
            default:
                console.log("algo non défini => algo jarvis utilisé");
                this.envconv = this.algoJarvis(this.points);
                break;
        }
    }

    static angle(p1,p2,candidate)
    {
        var l1 = new Coord2D(p2.x-p1.x,p2.y-p1.y);
        var l2 = new Coord2D(candidate.x-p2.x,candidate.y-p2.y);

        var angle = (l1.x*l2.x) + (l1.y*l2.y);
        
        angle = angle / (Math.abs(l1.x-l2.x) + Math.abs(l1.y-l2.y) );

        return angle;
    }

    static findNextIdx(before,previous, points,result) {

        var maxAngle = 0;
        var id = 0; 

        for(let i = 0; i < points.length;i++)
        {
            var angle = EnveloppeConvexe.angle(before,points[previous],points[i]);

            if(Coord2D.tour(before,points[previous],points[i]) < 0){
                if(angle > maxAngle && !result.includes(i))
                {
                    id = i; maxAngle = angle;
                }
            }
        }

        return id;
    }

 

    algoDemiPlan(points) {
    //todo: implementation de cet algo
    }

    algoJarvis(points) {
        var first = Coord2D.findMinIdx(points);
        var result = [first];
        var current;
        var previous = first;
        var before = new Coord2D(first.x,first.y - 1 );

        var i =0;
        while(current != first && i < points.length)
        {
            current = EnveloppeConvexe.findNextIdx(before,previous,points,result);
            result.push(current);
            before = new Coord2D(points[previous].x,points[previous].y - 1 );//before = previous;
            previous = current;
            i++;
        }

        this.envConv = result;
    }

    algoGraham() {
    //todo: implementation de cet algo
    }
}
